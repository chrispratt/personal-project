﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace SCE_CaptainAmerica.Controllers
{
    public class CreatePageController : ApiController
    {
        // GET: api/CreatePage
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        [AcceptVerbs("GET")]
        // GET: api/Auth/5
        // Used for loading templates into Create Page page
        public string[] files(string id)
        {
            // Specifies directory on the server //
            id = "~/Views/Templates";
            // Checks if user is authenticated //
            if (HttpContext.Current.User.Identity.IsAuthenticated)
            {
                // Loads files and returns them to user //
                List<string> files = new List<string>();
                DirectoryInfo dirInfo = new DirectoryInfo(System.Web.HttpContext.Current.Server.MapPath(id));
                if (dirInfo.Exists)
                {
                    foreach (FileInfo fInfo in dirInfo.GetFiles())
                    {
                        files.Add(fInfo.Name);
                    }
                    return files.ToArray();
                }
                else
                {
                    return null;
                }
            }
            else
            {
                return null;
            }
        }



        // POST: api/CreatePage
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/CreatePage/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/CreatePage/5
        public void Delete(int id)
        {
        }
    }
}
