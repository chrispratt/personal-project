﻿$(document).ready(function () {
    // Load form and append it to page //
    var htmlStr = '<div class="alert alert-danger" role="alert" style="display: none;"></div>'
    + '<form class="form-horizontal" method="post" action="../../CreatePage/CreatePage.aspx">'
    + '<div class="form-group">'
    + '<label for="pageName" class="col-sm-2 control-label">Page Name</label>'
    + '<div class="col-sm-9">'
    + '<div class="input-group">'
    + '<div class="input-group-addon" id="url-addon">.../</div>'
    + '<input type="text" class="form-control" name="pageName" id="pageName" aria-describedby="url-addon">'
    + '</div></div></div>'
    + '<div class="form-group">'
    + '<label for="template" class="col-sm-2 control-label">Template</label>'
    + '<div class="col-sm-9">'
    + '<select class="form-control" name="template" id="template" onchange="showNavBars(this.id)"></select>'
    + '</div></div>'
    + '<div class="form-group" id="navbars" style="display: none;">'
    + '<label for="template" class="col-sm-2 control-label">Side Navigation</label>'
    + '<div class="col-sm-9">'
    + '<select class="form-control" name="sideNav" id="sideNav"></select>'
    + '</div></div>'
    + '<div class="form-group" id="slideshows" style="display: none;">'
    + '<label for="template" class="col-sm-2 control-label">Slideshow</label>'
    + '<div class="col-sm-9">'
    + '<select class="form-control" name="slideshow" id="slideshow"></select>'
    + '</div></div>'
    + '<div class="form-group">'
    + '<div class="col-sm-offset-2 col-sm-10">'
    + '<input type="submit" value="Create Page" class="btn btn-success" />'
    + '</div></div></form>';
    $(".plugin-create-page").html(htmlStr);
    // Checks for authentication //
    $.getJSON("/api/Auth/Get/5", function (data) {
        if (data) {
            // Gets templates add adds them to the templates dropdown //
            $.getJSON("/api/CreatePage/files/5", function (value) {
                for (var i = 0; i < value.length; i++) {
                    if (!value[i].startsWith('_')) {
                        $("select[name='template']").append('<option value="' + value[i] + '">' + value[i].replace('.cshtml', '') + '</option>');
                    }
                }
            });
            // Checks for errors //
            if (gup("error") != "") {
                $(".alert-danger").css("display", "block");
                $(".alert-danger").html(gup("error"));
            }
        }
        else {
            alert("You are not authorized to access this page!");
            window.location.href = "/Main/Index";
        }
    });
    
});

// function checks for template //
function showNavBars(id) {
    // if template is side navigation, loads the possible side navigations bars //
    if ($("#" + id).val().replace(".cshtml", "") == "Side Navigation") {
        $("#navbars").css("display", "block");
        $("#slideshows").css("display", "none");
        $("select[name='sideNav']").find('option').remove();
        $.getJSON("/api/Navigations/navbars/5", function (value) {
            for (var i = 0; i < value.length; i++) {
                if (value[i] != "subsiteDefault.json")
                    $("select[name='sideNav']").append('<option value="' + value[i].replace('.json', '') + '">' + value[i].replace('.json', '') + '</option>');
            }
        });
    }
    // if the template is a slideshow page, loads the possible slideshows //
    else if ($("#" + id).val().replace(".cshtml", "") == "Slideshow Page") {
        $("#slideshows").css("display", "block");
        $("#navbars").css("display", "none");
        $("select[name='slideshows']").find('option').remove();
        $.getJSON("/api/Slideshow/get/5", function (value) {
            for (var i = 0; i < value.length; i++) {
                if (value[i] != "subsiteDefault.json")
                    $("select[name='slideshow']").append('<option value="' + value[i].replace('.json', '') + '">' + value[i].replace('.json', '') + '</option>');
            }
        });
    }

    // if the template is a normal page //
    else if ($("#" + id).val().replace(".cshtml", "") == "Basic Page") {
        $("#slideshows").css("display", "none");
        $("#navbars").css("display", "none");
    }
}
