﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SCE_CaptainAmerica.Controllers;
using System.IO;
using System.Text.RegularExpressions;

namespace SCE_CaptainAmerica
{
    public partial class CreatePage : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            // Gets values from form //
            string pageName = Request["pageName"].Replace(' ', '-');
            string template = Request["template"];
            string sideNav = Request["sideNav"];
            string slideshow = Request["slideshow"];
            
            // Checks to see if page exists, if so return error saying the page exists and to change the name of the page //
            if (!File.Exists(System.Web.HttpContext.Current.Server.MapPath("~/Views/Home/" + pageName + ".cshtml")))
            {
                // Gets contents of file and adds values from the form to certain spots //
                string htmlFile = File.ReadAllText(System.Web.HttpContext.Current.Server.MapPath("~/Views/Templates/" + template)).Replace("[title]", Request["pageName"]).Replace("[list]", sideNav).Replace("[slideshow]", slideshow);
                string[] html = File.ReadAllLines(System.Web.HttpContext.Current.Server.MapPath("~/Views/Templates/" + template));
                // Create the content areas //
                foreach (string line in html)
                {
                    var regex = new Regex(@"content\d");
                    var areas = regex.Match(line.ToLower());
                    if (areas.Success)
                    {
                        var path = Server.MapPath("~/textfiles/" + pageName + "_" + areas.Value + ".html");
                        System.IO.File.WriteAllText(path, "<h1>" + pageName + "</h1>");
                    }
                }
                // Save the new page //
                var path2 = Server.MapPath("~/Views/Home/" + pageName + ".cshtml");
                System.IO.File.WriteAllText(path2, htmlFile);
                Response.Redirect("~/" + pageName);
            }
            else
            {
                Response.Redirect("~/NewPage?error=A page with the same name already exists.  Please chose a different page name.");
            }
            
        }
    }
}