﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SCE_batman.Logout
{
    public partial class Logout : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            // Checks if user is already authenticated //
            if (HttpContext.Current.User.Identity.IsAuthenticated)
            {
                // Sign user out and redirect them to their page //
                FormsAuthentication.SignOut();
                Response.Redirect(Request["ReturnUrl"]);
            }
            else
            {
                // Redirect user to their page //
                Response.Redirect(Request["ReturnUrl"]);
            }
        }
    }
}