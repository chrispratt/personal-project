﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace SCE_CaptainAmerica
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "Default",
                url: "{subsite}/{id}",
                defaults: new { controller = "Home", action = "Pages", id = UrlParameter.Optional, subsite = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "NotFound",
                url: "{*.*}",   // Matches all paths, so that if it isn't already matched we can return our own 404.
                defaults: new { controller = "Home", action = "Pages", id = "PageNotFound" }
                );
        }
    }
}
