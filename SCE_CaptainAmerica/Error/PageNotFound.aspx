<%@ Page Language="C#" MasterPageFile="~/Error/Error.Master" CodeFile="PageNotFound.aspx.cs" AutoEventWireup="true" Title="404 - Not Found" Inherits="SCE_CaptainAmerica.Error.PageNotFound" %>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h1>Looks like you're lost...</h1>
    <h3 style="padding-left: 20px;">You can always come back <a href="/">home</a> though.</h3>
</asp:Content>
