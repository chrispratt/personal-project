<%@ Page Language="C#" MasterPageFile="~/Error/Error.Master" CodeFile="ServerError.aspx.cs" AutoEventWireup="true" Title="500 - Server Error" Inherits="SCE_CaptainAmerica.Error.ServerError" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h1>Oops!</h1>
    <h3 style="padding-left: 20px;">Something's broken!<br/>Please check your URL or try again later.</h3>
</asp:Content>
