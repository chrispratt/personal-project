﻿using System;


namespace SCE_CaptainAmerica.Error
{
    public partial class ServerError : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Context.Response.TrySkipIisCustomErrors = true;
            Response.StatusCode = 500;
        }
    }
}
