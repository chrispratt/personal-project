﻿/*
  ______                                  ______                         __                            __     
 /      \                                /      \                       /  |                          /  |    
/$$$$$$  |  ______   __     __  ______  /$$$$$$  |  ______   _______   _$$ |_     ______   _______   _$$ |_   
$$ \__$$/  /      \ /  \   /  |/      \ $$ |  $$/  /      \ /       \ / $$   |   /      \ /       \ / $$   |  
$$      \  $$$$$$  |$$  \ /$$//$$$$$$  |$$ |      /$$$$$$  |$$$$$$$  |$$$$$$/   /$$$$$$  |$$$$$$$  |$$$$$$/   
 $$$$$$  | /    $$ | $$  /$$/ $$    $$ |$$ |   __ $$ |  $$ |$$ |  $$ |  $$ | __ $$    $$ |$$ |  $$ |  $$ | __ 
/  \__$$ |/$$$$$$$ |  $$ $$/  $$$$$$$$/ $$ \__/  |$$ \__$$ |$$ |  $$ |  $$ |/  |$$$$$$$$/ $$ |  $$ |  $$ |/  |
$$    $$/ $$    $$ |   $$$/   $$       |$$    $$/ $$    $$/ $$ |  $$ |  $$  $$/ $$       |$$ |  $$ |  $$  $$/ 
 $$$$$$/   $$$$$$$/     $/     $$$$$$$/  $$$$$$/   $$$$$$/  $$/   $$/    $$$$/   $$$$$$$/ $$/   $$/    $$$$/  
                                                                                                              
                                                                                                              
    Created By: Josh Sorensen
    Modified: Septebmer 2016 
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SCE_batman.Core
{
    public partial class SaveContent : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            // Checks if the user is authorized to edit //
            if (HttpContext.Current.User.Identity.IsAuthenticated)
            {
                try
                {
                    string areaid = "";
                    string url = "";
                    string page = "";
                    string html = "";
                    string subsite = "";
                    // Get all of the values from the edit form //
                    foreach (string header in Request.Form.AllKeys)
                    {
                        // Regex here to prevent cross-site scripting //
                        Regex rRemScript = new Regex(@"<script[^>]*>[\s\S]*?</script>");
                        var output = rRemScript.Replace(Request.Form[header], "");
                        if (header.StartsWith("areaid"))
                        {
                            areaid = output;
                        }
                        else if (header.StartsWith("ReturnURL"))
                        {
                            url = output;
                        }
                        else if (header.StartsWith("page"))
                        {
                            page = output;

                        }
                        else if (header.StartsWith("subsite"))
                        {
                            subsite = output;
                        }
                        else if (header.StartsWith("mytextarea"))
                        {
                            html = output;
                        }
                    }

                    // Writes the html to the text file //
                    if (subsite == "Main")
                    {
                        var path = Server.MapPath("/textfiles/" + page + "_" + areaid + ".html");
                        System.IO.File.WriteAllText(path, html);
                    }
                    else
                    {
                        var path = Server.MapPath("~/plugins/Subsite/Subsites/" + subsite + "/textfiles/" + page + "_" + areaid + ".html");
                        System.IO.File.WriteAllText(path, html);
                    }

                    // Returns the user to the page they edited //
                    Response.Redirect(url);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            else
            {
                // If the user is not authorized to edit, they are sent to the home page //
                Response.Redirect("/Main/Index?errorMessage=Your changes were not saved!");
            }

        }
    }
}