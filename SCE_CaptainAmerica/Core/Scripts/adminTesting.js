﻿/***
 *     █████╗ ██████╗ ███╗   ███╗██╗███╗   ██╗        ██╗███████╗
 *    ██╔══██╗██╔══██╗████╗ ████║██║████╗  ██║        ██║██╔════╝
 *    ███████║██║  ██║██╔████╔██║██║██╔██╗ ██║        ██║███████╗
 *    ██╔══██║██║  ██║██║╚██╔╝██║██║██║╚██╗██║   ██   ██║╚════██║
 *    ██║  ██║██████╔╝██║ ╚═╝ ██║██║██║ ╚████║██╗╚█████╔╝███████║
 *    ╚═╝  ╚═╝╚═════╝ ╚═╝     ╚═╝╚═╝╚═╝  ╚═══╝╚═╝ ╚════╝ ╚══════╝
 *
 */

// Eventually we want users to be able to select images, files, or pages as links, any code involving the linkList or imageList involves adding this functionality //

var url = window.location.href; // Gets the location url of current page
var path = window.location.pathname; // Gets the location path of current page
var linkList = [];
var imageList = [];

var realPath = path.split("/");
path = realPath[realPath.length - 1];
// Gets the path down to the individual page (ex. path="/test.html" will go to path="home"). //
path = path.replace('/', '');
path = path.replace(".html", '');

var current = new Date();

// If there is nothing in the path, we are on the home page.
if (path.length < 1) {
    path = "Index";
}

$(document).ready(function () {
    /*$.getJSON("/api/document/GetDocumentDirectory", function (data) {
        console.log(data.directory.subDirectories);
        for (var i = 0; i < data.directory.subDirectories.Content/Images.Files.length; i++) {
            console.log(data.directory.subDirectories.Content/Images.Files[i]);
        }
    });*/
    if (gup("errorMessage") != "") {
        alert(gup("errorMessage"));
    }
    // Checks if user is authorized //
    $.getJSON("/api/Auth/Get/5", function (data) {
        if (data) {
            $.getJSON("/api/Auth/filesLink/5", function (value) {
                for (var i = 0; i < value.length; i++) {
                    var linkValue = "../../Home/Pages/" + value[i].replace(".cshtml", "");
                    var link = {
                        title: value[i].replace(".cshtml", ""),
                        value: linkValue
                    };
                        linkList.push(link);
                    }
            });

            $.getJSON("/api/document/GetDocumentDirectory", function (value) {
                for (var property in value.directory.subDirectories) {
                    if (property == "Content\\Images" || property == "Content\\Documents") {
                        var imageListMain = {
                            title: value.directory.subDirectories[property].path,
                            menu: []
                        }
                        for (var j = 0; j < value.directory.subDirectories[property].files.length; j++) {
                            if (value.directory.subDirectories[property].files[j].fileExt == "png" || value.directory.subDirectories[property].files[j].fileExt == "jpg" || value.directory.subDirectories[property].files[j].fileExt == "gif") {
                                var image = {
                                    title: value.directory.subDirectories[property].files[j].fileName + "." + value.directory.subDirectories[property].files[j].fileExt,
                                    value: "../../Content/Images/" + value.directory.subDirectories[property].files[j].fileName + "." + value.directory.subDirectories[property].files[j].fileExt
                                };
                                imageListMain.menu.push(image);
                            }
                        }
                        if (imageListMain.menu.length > 0) {
                            imageList.push(imageListMain);
                        }
                    }
                }
                
                /*for (var i = 0; i < directories.length; i++) {
                    var imageListMain = {
                        title: directories[i].path,
                        menu: []
                    };
                    
                }*/
                /*for (var i = 0; i < value.directory.subDirectories[i].files.length; i++) {
                    var image = {
                        title: value.directory.subDirectories[i].files[i].fileName + "." + value.directory.subDirectories[i].files[i].fileExt,
                        value: "../../Content/Images/" + value.directory.subDirectories[i].files[i].fileName + "." + value.directory.subDirectories[i].files[i].fileExt
                    };
                    imageList.push(image);
                }*/
            });

            // Iterates through each editible region on the page and loads the text file as the html, this is where the path varible is implemented //
            $("div[id^='content']").each(function (index) {
                var id = $(this).attr("id");
                $.ajax({
                    url: "/textFiles/" + path + "_content" + (index + 1) + ".html?version=" + current.toUTCString(),
                    dataType: "text",
                    success: function (data) {
                        /*
                            Adds data to area, also set the content area so when it is clicked you can edit it in the bootstrap modal.
                            The area has a border put around it, and a button is added to edit the content in a bootstrap modal.
                        */
                        var htmlStr = '<form method="post" id="saveContentForm' + index + '" action="/Core/SaveContent.aspx">'
                        + '<input type="text" name="areaid' + index + '" value="content' + (index + 1) + '" style="display: none;" />'
                        + '<input type="text" name="page' + index + '" value="' + path + '" style="display: none;" />'
                        + '<input type="text" name="ReturnURL' + index + '" value="' + url + '" style="display: none;" />'
                        + '<textarea style="height: 400px;" name="mytextarea' + index + '" id="mytextareacontent' + (index + 1) + '">' + data + '</textarea>'
                        + '<input type="submit" id="savecontent' + (index + 1) + '" class="btn btn-success" Value="Save" />'
                        + '</form>';
                        $("#content" + (index + 1)).html(htmlStr);
                        tinymce.init({
                            selector: 'textarea#mytextareacontent' + (index + 1),  // ID of textarea must match whatever is after the '#' //
                            plugin: 'a_tinymce_plugin',
                            plugins: 'advlist autolink link image media lists charmap print preview hr anchor pagebreak spellchecker searchreplace wordcount visualblocks visualchars fullscreen insertdatetime media nonbreaking save table contextmenu directionality emoticons template paste textcolor',
                            toolbar: 'undo redo | indent outdent | bold italic underline forecolor backcolor | alignleft aligncenter alignright | bullist numlist | link image media | file code',
                            theme: 'modern',
                            link_list: linkList,
                            image_list: imageList,
                            /*file_picker_callback: function (callback, value, meta) {
                                // Provide file and text for the link dialog
                                if (meta.filetype == 'file') {
                                    callback('../../Home/Pages/', { text: '' });
                                }
                                // Provide image and alt text for the image dialog
                                if (meta.filetype == 'image') {
                                    $('#imageModal' + (index + 1)).modal("show");
                                }
                            },*/
                            a_plugin_option: true,
                            a_configuration_option: 400,
                            //content_css: "../../Core/Styles/style.css"
                        });
                    },
                    error: function (error) {
                        $.post("/api/Auth/Post/" + path + "_" + id, function (item) {
                            console.log("created successfully!");
                            window.location.reload();
                        });
                    }
                });
            });
        }
        else {
            // For each editible content area, loads the content, but without the ability to edit pages. //
            $("div[id^='content']").each(function (index) {
                $.ajax({
                    url: "/textFiles/" + path + "_content" + (index + 1) + ".html?version=" + current.toUTCString(),
                    dataType: "text",
                    success: function (data) {
                        $("#content" + (index + 1)).html(data);
                    }
                });
            });
        }
    });
});

// Gets query string values //
function gup(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}