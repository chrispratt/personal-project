﻿/*
 __                            __  __    __                       __                        __      __                                           
/  |                          /  |/  \  /  |                     /  |                      /  |    /  |                                          
$$ |  ______    ______    ____$$ |$$  \ $$ |  ______   __     __ $$/   ______    ______   _$$ |_   $$/   ______   _______           __   _______ 
$$ | /      \  /      \  /    $$ |$$$  \$$ | /      \ /  \   /  |/  | /      \  /      \ / $$   |  /  | /      \ /       \         /  | /       |
$$ |/$$$$$$  | $$$$$$  |/$$$$$$$ |$$$$  $$ | $$$$$$  |$$  \ /$$/ $$ |/$$$$$$  | $$$$$$  |$$$$$$/   $$ |/$$$$$$  |$$$$$$$  |        $$/ /$$$$$$$/ 
$$ |$$ |  $$ | /    $$ |$$ |  $$ |$$ $$ $$ | /    $$ | $$  /$$/  $$ |$$ |  $$ | /    $$ |  $$ | __ $$ |$$ |  $$ |$$ |  $$ |        /  |$$      \ 
$$ |$$ \__$$ |/$$$$$$$ |$$ \__$$ |$$ |$$$$ |/$$$$$$$ |  $$ $$/   $$ |$$ \__$$ |/$$$$$$$ |  $$ |/  |$$ |$$ \__$$ |$$ |  $$ | __     $$ | $$$$$$  |
$$ |$$    $$/ $$    $$ |$$    $$ |$$ | $$$ |$$    $$ |   $$$/    $$ |$$    $$ |$$    $$ |  $$  $$/ $$ |$$    $$/ $$ |  $$ |/  |    $$ |/     $$/ 
$$/  $$$$$$/   $$$$$$$/  $$$$$$$/ $$/   $$/  $$$$$$$/     $/     $$/  $$$$$$$ | $$$$$$$/    $$$$/  $$/  $$$$$$/  $$/   $$/ $$/__   $$ |$$$$$$$/  
                                                                     /  \__$$ |                                              /  \__$$ |          
                                                                     $$    $$/                                               $$    $$/           
                                                                      $$$$$$/                                                 $$$$$$/            

    Created By: Josh Sorensen
    Modified: April 2017
*/

var url = window.location.href; // Gets the location url of current page
var path = window.location.pathname; // Gets the location path of current page
var subsite = "Main";

var realPath = path.split("/");
path = realPath[realPath.length - 1];
if (realPath.length > 2) {
    subsite = realPath[realPath.length - 2];
}

$(document).ready(function () {
    // Loads must recent version of navigation //
    var currentDate = new Date();
    if (subsite == "Main") {
        $.getJSON("/Core/Scripts/Navigation/Navigations/main.json?version=" + currentDate.toUTCString(), function (data) {
            // sorts navigation by display order //
            var navigationItems = data.Items;
            navigationItems = navigationItems.sort(function (a, b) {
                if (a.DisplayOrder < b.DisplayOrder)
                    return -1;
                if (a.DisplayOrder > b.DisplayOrder)
                    return 1;
                return 0;
            });
            // Parse the navigation, separated out code for a mega menu link and regular link //
            var htmlStr = "";
            for (var i = 0; i < navigationItems.length; i++) {
                if (navigationItems[i].MenuType == "Mega menu") {
                    htmlStr += '<li class="dropdown" style="text-align: center;">'
                    + '<h4><a class="dropdown-toggle" data-toggle="dropdown" href="#">' + navigationItems[i].Title + '</a></h4>'
                    + '<ul class="dropdown-menu dropdown-menu-left">'
                    + navigationItems[i].MegaMenuContent
                    + '</ul></li>';
                }
                else {
                    htmlStr += '<li><h4><a href="' + navigationItems[i].Link + '">' + navigationItems[i].Title + '</a></h4></li>';
                }
            }
            // Add code to the navigation //
            $("#mainNav").prepend(htmlStr);
            $("#mainNavMobile").prepend(htmlStr);

            $(document).trigger("sce:navloaded");
        });
    }
    else {
        $.getJSON("/Core/Scripts/Navigation/Navigations/" + subsite + "_main.json?version=" + currentDate.toUTCString(), function (data) {
            // sorts navigation by display order //
            var navigationItems = data.Items;
            navigationItems = navigationItems.sort(function (a, b) {
                if (a.DisplayOrder < b.DisplayOrder)
                    return -1;
                if (a.DisplayOrder > b.DisplayOrder)
                    return 1;
                return 0;
            });
            // Parse the navigation, separated out code for a mega menu link and regular link //
            var htmlStr = "";
            for (var i = 0; i < navigationItems.length; i++) {
                if (navigationItems[i].MenuType == "Mega menu") {
                    htmlStr += '<li class="menu-parent main-menu dropdown" style="text-align: center;">'
                    + '<a class="dropdown-toggle" data-toggle="dropdown" href="#">' + navigationItems[i].Title + '</a>'
                        + '<ul class="dropdown-menu dropdown-menu-left">'
                    + navigationItems[i].MegaMenuContent
                    + '</ul></li>';
                }
                else {
                    htmlStr += '<li class="menu-parent main-menu"><a href="' + navigationItems[i].Link + '">' + navigationItems[i].Title + '</a></li>';
                }
            }
            // Add code to the navigation //
            $("#mainNav").prepend(htmlStr);
            $("#mainNavMobile").prepend(htmlStr);

            $(document).trigger("sce:navloaded");
        });
    }
    // This script allows for dropdown in dropdown menus to work //
        $('ul.dropdown-menu [data-toggle=dropdown]').on('click', function (event) {
            event.preventDefault();
            event.stopPropagation();
            $(this).parent().siblings().removeClass('open');
            $(this).parent().toggleClass('open');
        });
});

