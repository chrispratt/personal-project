﻿/*
 ________        __  __    __            __    __                                        
/        |      /  |/  |  /  |          /  \  /  |                                       
$$$$$$$$/   ____$$ |$$/  _$$ |_         $$  \ $$ |  ______   __     __      __   _______ 
$$ |__     /    $$ |/  |/ $$   |        $$$  \$$ | /      \ /  \   /  |    /  | /       |
$$    |   /$$$$$$$ |$$ |$$$$$$/         $$$$  $$ | $$$$$$  |$$  \ /$$/     $$/ /$$$$$$$/ 
$$$$$/    $$ |  $$ |$$ |  $$ | __       $$ $$ $$ | /    $$ | $$  /$$/      /  |$$      \ 
$$ |_____ $$ \__$$ |$$ |  $$ |/  |      $$ |$$$$ |/$$$$$$$ |  $$ $$/__     $$ | $$$$$$  |
$$       |$$    $$ |$$ |  $$  $$/       $$ | $$$ |$$    $$ |   $$$//  |    $$ |/     $$/ 
$$$$$$$$/  $$$$$$$/ $$/    $$$$/        $$/   $$/  $$$$$$$/     $/ $$/__   $$ |$$$$$$$/  
                                                                     /  \__$$ |          
                                                                     $$    $$/           
                                                                      $$$$$$/          
                                                                      
   By: Josh Sorensen
   Modified: November 22, 2016
                                                                       
*/

$(document).ready(function () {

    // Load form //
    var beforeStr = ''
    + '<div class="alert alert-success" role="alert" style="display: none;"></div>'
    + '<div class="alert alert-danger" role="alert" style="display: none;"></div>'
    + '<form class="form-horizontal" method="post" id="editNavigation" action="/Core/Scripts/Navigation/SaveNavigation.aspx">'
    + '<div class="form-group"><label for="[name=\'navbars\']" class="col-sm-2 control-label">Select Navigation</label><div class="col-sm-8"><select class="form-control" name="navbars"></select></div></div>'
    + '<div class="form-group" id="newNavInput"><label for="[name=\'navBarNewName\']" class="col-sm-2 control-label">Navigation Name</label><div class="col-sm-8"><input class="form-control" name="newNav" type="text" /></div></div>'
    + '<div class="col-sm-offset-2 col-sm-8"><input type="button" id="delete" value="Delete Navigation" class="btn btn-danger" onclick="deleteNavigation()" /><br/><br/></div><br/>'
    + '<div class="navitems"></div>'
    + '<div class="form-group" id="navigationSubmit">'
    + '<div class="col-sm-offset-2 col-sm-8">'
    + '<input type="submit" value="Save Changes" class="btn btn-success" />'
    + '<input type="button" id="cancel" value="Cancel" class="btn btn-danger" />'
    + '</div></div></form>';
    $(".core-navigation-edit").html(beforeStr);

    // Loads all navigation lists //
    $.getJSON("/api/Navigations/navbars/5", function (value) {
        $("select[name='navbars']").append('<option value="new">Create New Navigation</option>');
        // Checks to see if any navigations are there //
        if (value.length > 0) {
            for (var i = 0; i < value.length; i++) {
                if (value[i] != "subsiteDefault.json")
                    $("select[name='navbars']").append('<option value="' + value[i].replace('.json', '') + '">' + value[i].replace('.json', '') + '</option>');
            }
            // Checks if main.json navigation file exists, if it does not by default the next navigation is selected and loaded //
            if (jQuery.inArray("main.json", value) !== -1) {
                $("select[name='navbars']").val("main");
                $("#delete").prop("disabled", "");
                $("#newNavInput").css('display', 'none');
                loadForm();
            }
            else {
                $("select[name='navbars']").prop("selectedIndex", 1);
                $("#delete").prop("disabled", "");
                $("#newNavInput").css('display', 'none');
                loadForm();
            }
        }
        else {
            // New navigation form loads by default if no navigations are there //
            $("select[name='navbars']").val("new");
            $("#delete").prop("disabled", "disabled");
            $("#newNavInput").css('display', 'block');
            loadNewForm();
        }
    });
   
    // Checks for errors //
    if (gup("error") != "") {
        $(".alert-danger").css("display", "block");
        $(".alert-danger").html(gup("error"));
    }

    // Successful changes //
    if (gup("success") != "") {
        $(".alert-success").css("display", "block");
        $(".alert-success").html(gup("success"));
    }

    // When the navigations dropdown value changes this function runs //
    $("select[name='navbars']").change(function () {
        // If the navigation selected is new, the new navigation form is loaded //
        if ($("select[name='navbars']").val() == "new") {
            $("#delete").prop("disabled", "disabled");
            $("#newNavInput").css('display', 'block');
            loadNewForm();
        }
        else {
            $("#newNavInput").css('display', 'none');
            $("#delete").prop("disabled", "");
            loadForm();
        }
    });
});

function loadForm() {
    // Checks for authentication, if user is not not authorized, they will be returned to the Home Page //
    $.getJSON("/api/Auth/Get/5", function (value) {
        if (value) {
            var currentDate = new Date();

            // Loads navigation from json file and populates editing area //
            $.getJSON("../../Core/Scripts/Navigation/Navigations/" + $("select[name='navbars']").val() + ".json?version=" + currentDate.toUTCString(), function (data) {
                var htmlStr = "";
                for (var i = 0; i < data.Items.length; i++) {
                    var selectedLink = " selected";
                    var selectedMegaMenu = "";
                    if (data.Items[i].MenuType != "Link") {
                        selectedLink = "";
                        selectedMegaMenu = " selected";
                    }

                    htmlStr += "<div id='tableRow" + i + "'><div class='form-group'><div class='col-sm-offset-2 col-sm-8'><h2>Navigation item " + (i + 1) + "</h2></div></div>" +
                                "<div class='form-group'><label for='[name=\"title" + i + "\"]' class='col-sm-2 control-label'>Title</label><div class='col-sm-8'><input class='form-control' type='text' name='title" + i + "' value='" + data.Items[i].Title + "' /></div></div>" +
                                "<div class='form-group'><label for='[name=\"menuType" + i + "\"]' class='col-sm-2 control-label'>Menu Type</label><div class='col-sm-8'><select class='form-control' name='menuType" + i + "'><option value='Link'" + selectedLink + ">Link</option><option value='Mega menu'" + selectedMegaMenu + ">Mega menu</option></select></div></div>" +
                                "<div class='form-group'><label for='[name=\"link" + i + "\"]' class='col-sm-2 control-label'>Link</label><div class='col-sm-8'><input class='form-control' type='text' name='link" + i + "' value='" + data.Items[i].Link + "' /></div></div>" +
                                "<div class='form-group'><label for='[name=\"megaMenuContent" + i + "\"]' class='col-sm-2 control-label'>Mega menu content</label><div class='col-sm-8'><textarea class='form-control' rows='6' cols='110' name='megaMenuContent" + i + "'>" + data.Items[i].MegaMenuContent + "</textarea></div></div>" +
                                "<div class='form-group'><label for='[name=\"displayOrder" + i + "\"]' class='col-sm-2 control-label'>Display order:</label><div class='col-sm-8'><input class='form-control' type='text' name='displayOrder" + i + "' value='" + data.Items[i].DisplayOrder + "' onchange='displayOrderValidate(this.name)' /></div></div>" +
                                "<div class='form-group'><div class='col-sm-offset-2 col-sm-8'><button type='button' id='delete" + i + "' class='btn btn-danger' onclick='deleteNav(this.id)'>Remove Navigation " + (i + 1) + "</button></div></div><hr/></div>";

                    if (i == data.Items.length - 1) {
                        htmlStr += "<div class='form-group remove'><div class='col-sm-offset-2 col-sm-8'><button type='button' id='add" + i + "' class='btn btn-default' onclick='addNav(this.id)'><span class='glyphicon glyphicon-plus'></span>Add Navigation</button></div><hr/></div>";
                    }
                }
                $(".navitems").html(htmlStr);
            });

            $("#cancel").click(function () {
                window.location.href = "/";
            });
        }
        else {
            // Anonymous users are sent back to home page //
            alert("You are not authorized to access this page.");
            window.location.href = "/";
        }
    });
}

// Loads new navigation form //
function loadNewForm() {
    var htmlStr = "";
    var selectedLink = " selected";
    var selectedMegaMenu = "";
    var i = 0;

    htmlStr += "<div id='tableRow" + i + "'><div class='form-group'><div class='col-sm-offset-2 col-sm-8'><h2>Navigation item " + (i + 1) + "</h2></div></div>" +
                "<div class='form-group'><label for='[name=\"title" + i + "\"]' class='col-sm-2 control-label'>Title</label><div class='col-sm-8'><input class='form-control' type='text' name='title" + i + "' value='' /></div></div>" +
                "<div class='form-group'><label for='[name=\"menuType" + i + "\"]' class='col-sm-2 control-label'>Menu Type</label><div class='col-sm-8'><select class='form-control' name='menuType" + i + "'><option value='Link'" + selectedLink + ">Link</option><option value='Mega menu'" + selectedMegaMenu + ">Mega menu</option></select></div></div>" +
                "<div class='form-group'><label for='[name=\"link" + i + "\"]' class='col-sm-2 control-label'>Link</label><div class='col-sm-8'><input class='form-control' type='text' name='link" + i + "' value='' /></div></div>" +
                "<div class='form-group'><label for='[name=\"megaMenuContent" + i + "\"]' class='col-sm-2 control-label'>Mega menu content</label><div class='col-sm-8'><textarea class='form-control' rows='6' cols='110' name='megaMenuContent" + i + "'></textarea></div></div>" +
                "<div class='form-group'><label for='[name=\"displayOrder" + i + "\"]' class='col-sm-2 control-label'>Display order:</label><div class='col-sm-8'><input class='form-control' type='text' name='displayOrder" + i + "' value='' onchange='displayOrderValidate(this.name)' /></div></div>" +
                "<div class='form-group'><div class='col-sm-offset-2 col-sm-8'><button type='button' id='delete" + i + "' class='btn btn-danger' onclick='deleteNav(this.id)'>Remove Navigation " + (i + 1) + "</button></div></div><hr/></div>" +
                "<div class='form-group remove'><div class='col-sm-offset-2 col-sm-8'><button type='button' id='add" + i + "' class='btn btn-default' onclick='addNav(this.id)'><span class='glyphicon glyphicon-plus'></span>Add Navigation</button></div><hr/></div>";

    $(".navitems").html(htmlStr);
}

// Adds new navigation //
function addNav(id) {
    // Removes add navigation button on last id //
    $("#" + id + ", .remove").remove();
    id = id.substr(3, id.length);
    // Adds to last id //
    newid = Number(id) + 1;

    var htmlStr = "<div id='tableRow" + newid + "'><div class='form-group'><div class='col-sm-offset-2 col-sm-8'><h2>Navigation item " + (newid + 1) + "</h2></div></div>" +
                        "<div class='form-group'><label for='[name=\"title" + newid + "\"]' class='col-sm-2 control-label'>Title</label><div class='col-sm-8'><input class='form-control' type='text' name='title" + newid + "' /></div></div>" +
                        "<div class='form-group'><label for='[name=\"menuType" + newid + "\"]' class='col-sm-2 control-label'>Menu type</label><div class='col-sm-8'><select class='form-control' name='menuType" + newid + "'><option value='Link'>Link</option><option value='Mega menu'>Mega menu</option></select></div></div>" +
                        "<div class='form-group'><label for='[name=\"link" + newid + "\"]' class='col-sm-2 control-label'>Link</label><div class='col-sm-8'><input class='form-control' type='text' name='link" + newid + "' /></div></div>" +
                        "<div class='form-group'><label for='[name=\"megaMenuContent" + newid + "\"]' class='col-sm-2 control-label'>Mega menu content</label><div class='col-sm-8'><textarea class='form-control' rows='6' cols='110' name='megaMenuContent" + newid + "'></textarea></div></div>" +
                        "<div class='form-group'><label for='[name=\"displayOrder" + newid + "\"]' class='col-sm-2 control-label'>Display order</label><div class='col-sm-8'><input class='form-control' type='text' name='displayOrder" + newid + "' onchange='displayOrderValidate(this.name)' /></div></div>" +
                        "<div class='form-group'><div class='col-sm-offset-2 col-sm-8'><button type='button' id='delete" + newid + "' class='btn btn-danger' onclick='deleteNav(this.id)'>Remove Navigation " + (newid + 1) + "</button></div></div><hr/></div>";

    htmlStr += "<div class='form-group remove'><div class='col-sm-offset-2 col-sm-8'><button type='button' id='add" + newid + "' class='btn btn-default' onclick='addNav(this.id)'><span class='glyphicon glyphicon-plus'></span>Add Navigation</button></div><hr/></div>";

    $(".navitems").append(htmlStr);
}
function deleteNav(id) {

    // Deletes selected navigation //
    $("#" + id + ", .remove").remove();
    id = id.substr(6, id.length);

    $("#editNavigation #tableRow" + id).remove();

    var htmlStr = "<div class='form-group remove'><div class='col-sm-offset-2 col-sm-8'><button type='button' id='add" + (id-1) + "' class='btn btn-default' onclick='addNav(this.id)'><span class='glyphicon glyphicon-plus'></span>Add Navigation</button></div><hr/></div>";
    $("#editNavigation #tableRow" + (id-1)).append(htmlStr);

}

// Makes sure the display order field has unique values and is a number //
function displayOrderValidate(name) {
    if (isNaN($("input[name='" + name + "']").val())) {
        alert("This field must be a number!");
        $("input[name='" + name + "']").css("border-color", "red");
        $("input[name='" + name + "']").focus();
    }
    else {
        $("input[name='" + name + "']").css("border-color", "#ccc");
        $("input[name^='displayOrder']").each(function (item) {
            if (this.name != name) {
                if (this.value == $("input[name='" + name + "']").val()) {
                    alert("An item in the navigation already is already #" + $("input[name='" + name + "']").val() + ", please reorder items.");
                    $("input[name='" + name + "']").css("border-color", "red");
                    $("input[name='" + name + "']").focus();
                }
                else {
                    $("input[name='" + name + "']").css("border-color", "#ccc");
                }
            }
        })
    }
}

// This function deletes the selected navigation bar //
function deleteNavigation() {
    $.ajax({
        method: "DELETE",
        url: "/api/Navigations/Delete/" + $("select[name='navbars']").val(),
        success: function (data) {
            if (!data) {
                // If delete was unsuccessful, an error is returned to the user //
                window.location.href = "/Main/EditNavigation?error=An error occurred while deleting the navigation " + $("select[name='navbars']").val() + ".";
            }
            else {
                window.location.reload();
            }
        }
    });
}