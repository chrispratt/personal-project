﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace SCE_CaptainAmerica.Core.Scripts.Navigation.Controllers
{
    public class NavigationsController : ApiController
    {
        // GET: api/Navigations
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/CreatePage/5
        [AcceptVerbs("GET")]
        public string[] navbars(string id)
        {
            id = "~/Core/Scripts/Navigation/Navigations";
            if (HttpContext.Current.User.Identity.IsAuthenticated)
            {
                List<string> files = new List<string>();
                DirectoryInfo dirInfo = new DirectoryInfo(System.Web.HttpContext.Current.Server.MapPath(id));
                if (dirInfo.Exists)
                {
                    foreach (FileInfo fInfo in dirInfo.GetFiles())
                    {
                        files.Add(fInfo.Name);
                    }
                    return files.ToArray();
                }
                else
                {
                    return null;
                }
            }
            else
            {
                return null;
            }
        }

        // POST: api/Navigations
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/Navigations/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/Navigations/5
        public bool Delete(string id)
        {
            if (File.Exists(System.Web.HttpContext.Current.Server.MapPath("~/Core/Scripts/Navigation/Navigations/" + id + ".json"))) {
                File.Delete(System.Web.HttpContext.Current.Server.MapPath("~/Core/Scripts/Navigation/Navigations/" + id + ".json"));
                return true;
            }
            return false;
        }
    }
}
