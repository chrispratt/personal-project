﻿/*
  ______                                 __    __                       __                        __      __                     
 /      \                               /  \  /  |                     /  |                      /  |    /  |                    
/$$$$$$  |  ______   __     __  ______  $$  \ $$ |  ______   __     __ $$/   ______    ______   _$$ |_   $$/   ______   _______  
$$ \__$$/  /      \ /  \   /  |/      \ $$$  \$$ | /      \ /  \   /  |/  | /      \  /      \ / $$   |  /  | /      \ /       \ 
$$      \  $$$$$$  |$$  \ /$$//$$$$$$  |$$$$  $$ | $$$$$$  |$$  \ /$$/ $$ |/$$$$$$  | $$$$$$  |$$$$$$/   $$ |/$$$$$$  |$$$$$$$  |
 $$$$$$  | /    $$ | $$  /$$/ $$    $$ |$$ $$ $$ | /    $$ | $$  /$$/  $$ |$$ |  $$ | /    $$ |  $$ | __ $$ |$$ |  $$ |$$ |  $$ |
/  \__$$ |/$$$$$$$ |  $$ $$/  $$$$$$$$/ $$ |$$$$ |/$$$$$$$ |  $$ $$/   $$ |$$ \__$$ |/$$$$$$$ |  $$ |/  |$$ |$$ \__$$ |$$ |  $$ |
$$    $$/ $$    $$ |   $$$/   $$       |$$ | $$$ |$$    $$ |   $$$/    $$ |$$    $$ |$$    $$ |  $$  $$/ $$ |$$    $$/ $$ |  $$ |
 $$$$$$/   $$$$$$$/     $/     $$$$$$$/ $$/   $$/  $$$$$$$/     $/     $$/  $$$$$$$ | $$$$$$$/    $$$$/  $$/  $$$$$$/  $$/   $$/ 
                                                                           /  \__$$ |                                            
                                                                           $$    $$/                                             
                                                                            $$$$$$/                                              
    Created By: Josh Sorensen
    Modified: November 22, 2016
 
 */

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SCE_batman.Core
{
    public partial class SaveNavigation : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (HttpContext.Current.User.Identity.IsAuthenticated)
            {
                // Gets all the values from the Request Form and puts them in lists //
                List<string> title = new List<string>();
                List<string> menuType = new List<string>();
                List<string> link = new List<string>();
                List<string> megaMenuContent = new List<string>();
                List<int> displayOrder = new List<int>();
                string navbar = "";
                string newNavName = "";
                foreach (string header in Request.Form.AllKeys)
                {
                    // Regex here to prevent cross-site scripting //
                    Regex rRemScript = new Regex(@"<script[^>]*>[\s\S]*?</script>");
                    var output = rRemScript.Replace(Request.Form[header], "");
                    output = output.Replace("\\", "\\\\");
                    if (header.StartsWith("title"))
                    {
                        title.Add(output);
                    }
                    else if (header.StartsWith("menuType"))
                    {
                        menuType.Add(output);
                    }
                    else if (header.StartsWith("link"))
                    {
                        link.Add(output);
                    }
                    else if (header.StartsWith("megaMenuContent"))
                    {
                        megaMenuContent.Add(output.Replace("\"", "\\\"").Replace("\r\n", ""));
                    }
                    else if (header.StartsWith("displayOrder"))
                    {
                        displayOrder.Add(Convert.ToInt32(output));
                    }
                    else if (header.StartsWith("navbars"))
                    {
                        navbar = output;
                    }
                    else if (header.StartsWith("newNav"))
                    {
                        newNavName = output;
                    }
                }

                // Set the navigation file name to the navigation file being edited //
                string fileName = navbar + ".json";

                // When a new navigation is created, checks to see if the navigation is already there //
                if (navbar == "new")
                {
                    fileName = newNavName + ".json";
                    if (File.Exists(System.Web.HttpContext.Current.Server.MapPath("~/Core/Scripts/Navigation/Navigations/" + fileName))) {
                        // Error message returned because navigation already exists //
                        Response.Redirect("~/Main/EditNavigation?error=A navigation with this name already exists!");
                    }
                }

                string json = "{ \"Items\": [";

                // Iterates through the lists and adds them to the json string //
                for (int i = 0; i < title.Count; i++)
                {
                    if (i == title.Count - 1)
                    {
                        json += "{ \"Title\": \"" + title[i] + "\", \"MenuType\": \"" + menuType[i] + "\", \"Link\": \"" + link[i] + "\", \"MegaMenuContent\": \"" + megaMenuContent[i] + "\", \"DisplayOrder\": " + displayOrder[i] + " }\n";

                    }
                    else
                    {
                        json += "{ \"Title\": \"" + title[i] + "\", \"MenuType\": \"" + menuType[i] + "\", \"Link\": \"" + link[i] + "\", \"MegaMenuContent\": \"" + megaMenuContent[i] + "\", \"DisplayOrder\": " + displayOrder[i] + " },\n";
                    }
                }
                json += "] }";

                // Saves json to text file //
                var path = Server.MapPath("~/Core/Scripts/Navigation/Navigations/" + fileName);
                System.IO.File.WriteAllText(path, json);

                Response.Redirect("~/Main/EditNavigation?success=The navigation was saved successfully!");
            }
            else
            {
                // If the navigation failed to save, it is because they are logged out and they will receive an error //
                Response.Redirect("~/Main/EditNavigation?error=There was an error while saving the navigation!");
            }
        }
    }
}