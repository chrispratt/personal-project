﻿/*
 __                            __  __    __                       __                        __      __                                           
/  |                          /  |/  \  /  |                     /  |                      /  |    /  |                                          
$$ |  ______    ______    ____$$ |$$  \ $$ |  ______   __     __ $$/   ______    ______   _$$ |_   $$/   ______   _______           __   _______ 
$$ | /      \  /      \  /    $$ |$$$  \$$ | /      \ /  \   /  |/  | /      \  /      \ / $$   |  /  | /      \ /       \         /  | /       |
$$ |/$$$$$$  | $$$$$$  |/$$$$$$$ |$$$$  $$ | $$$$$$  |$$  \ /$$/ $$ |/$$$$$$  | $$$$$$  |$$$$$$/   $$ |/$$$$$$  |$$$$$$$  |        $$/ /$$$$$$$/ 
$$ |$$ |  $$ | /    $$ |$$ |  $$ |$$ $$ $$ | /    $$ | $$  /$$/  $$ |$$ |  $$ | /    $$ |  $$ | __ $$ |$$ |  $$ |$$ |  $$ |        /  |$$      \ 
$$ |$$ \__$$ |/$$$$$$$ |$$ \__$$ |$$ |$$$$ |/$$$$$$$ |  $$ $$/   $$ |$$ \__$$ |/$$$$$$$ |  $$ |/  |$$ |$$ \__$$ |$$ |  $$ | __     $$ | $$$$$$  |
$$ |$$    $$/ $$    $$ |$$    $$ |$$ | $$$ |$$    $$ |   $$$/    $$ |$$    $$ |$$    $$ |  $$  $$/ $$ |$$    $$/ $$ |  $$ |/  |    $$ |/     $$/ 
$$/  $$$$$$/   $$$$$$$/  $$$$$$$/ $$/   $$/  $$$$$$$/     $/     $$/  $$$$$$$ | $$$$$$$/    $$$$/  $$/  $$$$$$/  $$/   $$/ $$/__   $$ |$$$$$$$/  
                                                                     /  \__$$ |                                              /  \__$$ |          
                                                                     $$    $$/                                               $$    $$/           
                                                                      $$$$$$/                                                 $$$$$$/            

    Created By: Josh Sorensen
    Modified: November 22, 2016
*/

var url = window.location.href; // Gets the location url of current page
var path = window.location.pathname; // Gets the location path of current page

var realPath = path.split("/");
path = realPath[realPath.length - 1];
// Gets the path down to the individual page (ex. path="/test.html" will go to path="home"). //
path = path.replace('/', '');
path = path.replace(".html", '');

// If there is nothing in the path, we are on the home page.
if (path.length < 1) {
    path = "Index";
}

$(document).ready(function () {
    // Loads must recent version of navigation //
    var currentDate = new Date();
    $.getJSON("../../Core/Scripts/Navigation/Navigations/" + $(".side-nav").attr("data-list") + ".json?version=" + currentDate.toUTCString(), function (data) {
        // sorts navigation by display order //
        var navigationItems = data.Items;
        navigationItems = navigationItems.sort(function (a, b) {
            if (a.DisplayOrder < b.DisplayOrder)
                return -1;
            if (a.DisplayOrder > b.DisplayOrder)
                return 1;
            return 0;
        });
        // Parse the navigation, separated out code for a mega menu link and regular link //
        var htmlStr = "<ul>";
        for (var i = 0; i < navigationItems.length; i++) {
            if (navigationItems[i].MenuType == "Mega menu") {
                htmlStr += '<li>'
                + navigationItems[i].Title
                + '<ul>'
                + navigationItems[i].MegaMenuContent
                + '</ul></li>';
            }
            else {
                htmlStr += '<li><a href="' + navigationItems[i].Link + '">' + navigationItems[i].Title + '</a></li>';
            }
        }
        htmlStr += "</ul>";
        // Add code to the navigation //
        $(".side-nav").html(htmlStr);
        $(".side-nav, .side-nav ul").css("list-style-type", "none");
        $.each($(".side-nav ul li a"), function (index, value) {
            if ($(this).prop("href") == url) {
                $(this).addClass("highlight");
            }
        });
    });
    $('ul.dropdown-menu [data-toggle=dropdown]').on('click', function (event) {
        event.preventDefault();
        event.stopPropagation();
        $(this).parent().siblings().removeClass('open');
        $(this).parent().toggleClass('open');
    });
});

