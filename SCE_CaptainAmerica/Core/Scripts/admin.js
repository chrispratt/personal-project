﻿/***
 *     █████╗ ██████╗ ███╗   ███╗██╗███╗   ██╗        ██╗███████╗
 *    ██╔══██╗██╔══██╗████╗ ████║██║████╗  ██║        ██║██╔════╝
 *    ███████║██║  ██║██╔████╔██║██║██╔██╗ ██║        ██║███████╗
 *    ██╔══██║██║  ██║██║╚██╔╝██║██║██║╚██╗██║   ██   ██║╚════██║
 *    ██║  ██║██████╔╝██║ ╚═╝ ██║██║██║ ╚████║██╗╚█████╔╝███████║
 *    ╚═╝  ╚═╝╚═════╝ ╚═╝     ╚═╝╚═╝╚═╝  ╚═══╝╚═╝ ╚════╝ ╚══════╝
 *
 */

var url = window.location.href; // Gets the location url of current page
var path = window.location.pathname; // Gets the location path of current page
var subsite = "Main";

var realPath = path.split("/");
path = realPath[realPath.length - 1];
if (realPath.length > 2) {
    subsite = realPath[realPath.length - 2];
}
// Gets the path down to the individual page (ex. path="/test.html" will go to path="home"). //
path = path.replace('/', '');
path = path.replace(".html", '');

var current = new Date();

// If there is nothing in the path, we are on the home page.
if (path.length < 1) {
    path = "Index";
}

$(document).ready(function () {
    $.getJSON("/api/document/GetDocumentDirectory", function (data) {
        console.log(data.directory.subDirectories);
        for (var i = 0; i < data.directory.subDirectories.Content/Images.Files.length; i++) {
            console.log(data.directory.subDirectories.Content/Images.Files[i]);
        }
    });
    if (gup("errorMessage") != "") {
        alert(gup("errorMessage"));
    }
    // Checks if user is authorized //
    $.getJSON("/api/Auth/Get/5", function (data) {
        if (data) {
            // Iterates through each editible region on the page and loads the text file as the html, this is where the path varible is implemented //
            $("div[id^='content']").each(function (index) {
                var id = $(this).attr("id");
                var contentAreaURL = "";
                if (subsite != "Main") {
                    contentAreaURL = "/plugins/Subsite/Subsites/" + subsite;
                }
                $.ajax({
                    url: contentAreaURL + "/textFiles/" + path + "_content" + (index + 1) + ".html?version=" + current.toUTCString(),
                    dataType: "text",
                    success: function (data) {
                        /*
                            Adds data to area, also set the content area so when it is clicked you can edit it in the bootstrap modal.
                            The area has a border put around it, and a button is added to edit the content in a bootstrap modal.
                        */
                        $("#content" + (index + 1)).html(data);
                        $("#content" + (index + 1)).wrap('<div id="content' + (index + 1) + 'edit" style=" padding-right: 0px; padding-left: 0px;" class="' + $("#content" + (index + 1)).attr('class') + '"></div>')
                        $("#content" + (index + 1)).attr('class', 'col-md-12');
                        $("#content" + (index + 1)).css('border', '2px dashed #ccc');
                        $("#content" + (index + 1)).attr('data-toggle', 'modal');
                        $("#content" + (index + 1)).attr('data-target', '#myModalcontent' + (index + 1));
                        $("#content" + (index + 1)).mouseover(function () {
                            $(this).css('opacity', 0.8);
                            $(this).css('background-color', '#eee');
                            $(this).css('cursor', 'pointer');
                        })
                        $("#content" + (index + 1)).mouseout(function () {
                            $(this).css('background-color', 'transparent');
                            $(this).css('opacity', 1);
                        });
                        var htmlStr = '<button id="button' + (index + 1) + '" class="editButton btn btn-default" data-toggle="modal" data-target="#myModalcontent' + (index + 1) + '"><span class="glyphicon glyphicon-pencil" style="padding-right: 1em;"></span>Edit Content ' + (index + 1) + '</button><br/><br/>';
                        htmlStr += '<div id="myModalcontent' + (index + 1) + '" class="modal fade" role="dialog">';
                        htmlStr += '<div class="modal-dialog" style="width: 1000px;">';
                        htmlStr += '<!-- Modal content-->';
                        htmlStr += '<div class="modal-content">';
                        htmlStr += '<div class="modal-header">';
                        htmlStr += '<span style="float:inherit;margin-left: 60%;">Powered By: <img src="https://fhssutils.byu.edu/Images/SCE-logos/SCE_CaptainAmerica.png" width="90" /></span>';
                        htmlStr += '<button type="button" class="close" data-dismiss="modal">&times;</button>';
                        htmlStr += '<h4 class="modal-title" style="float: left;">Edit Section</h4>';
                        htmlStr += '</div>';
                        htmlStr += '<form method="post" id="saveContentForm' + index + '" action="/Core/SaveContent.aspx">';
                        htmlStr += '<div class="modal-body">';
                        htmlStr += '<input type="text" name="areaid' + index + '" value="content' + (index + 1) + '" style="display: none;" />';
                        htmlStr += '<input type="text" name="page' + index + '" value="' + path + '" style="display: none;" />';
                        htmlStr += '<input type="text" name="subsite' + index + '" value="' + subsite + '" style="display: none;" />';
                        htmlStr += '<input type="text" name="ReturnURL' + index + '" value="' + url + '" style="display: none;" />';
                        // Loads textarea where data will be available for editing.  NOTE: Make sure textarea id matches the tinymce.init.selector id. //
                        htmlStr += '<textarea style="height: 400px;" name="mytextarea' + index + '" id="mytextareacontent' + (index + 1) + '">' + $("#content" + (index + 1)).html() + '</textarea>';
                        htmlStr += '</div>';
                        htmlStr += '<div class="modal-footer">';
                        htmlStr += '<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>';
                        htmlStr += '<input type="submit" id="savecontent' + (index + 1) + '" class="btn btn-success" Value="Save" />';
                        htmlStr += '</div>';
                        htmlStr += '</form>';
                        htmlStr += '</div>';
                        htmlStr += '</div>';
                        htmlStr += '</div>';
                        htmlStr += '</div>';
                        $("#content" + (index + 1)).after(htmlStr);
                        $("input[name='areaid']").val("content" + (index + 1));
                        $("input[name='page']").val(path);
                        $("input[name='ReturnURL']").val(url);
                        // Loads the tinymce text editor //
                        tinymce.init({
                            selector: 'textarea#mytextareacontent' + (index + 1),  // ID of textarea must match whatever is after the '#' //
                            plugin: 'a_tinymce_plugin',
                            plugins: 'advlist autolink link image media lists charmap print preview hr anchor pagebreak spellchecker searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking save table contextmenu directionality emoticons template paste textcolor',
                            toolbar: 'undo redo | indent outdent | bold italic underline forecolor backcolor | alignleft aligncenter alignright | bullist numlist | link image media | file code',
                            theme: 'modern',
                            a_plugin_option: true,
                            a_configuration_option: 400
                        });
                        $("#mceu_44-action").click();
                    },
                    error: function (error) {
                        if (subsite != "Main") {
                            $.post("/api/Auth/Post/" + subsite + "_" + path + "_" + id, function (item) {
                                console.log("created successfully!");
                                window.location.reload();
                            });
                        }
                    }
                });
            });
        }
        else {
            // For each editible content area, loads the content, but without the ability to edit pages. //
            $("div[id^='content']").each(function (index) {
                var contentAreaURL = "";
                if (subsite != "Main") {
                    contentAreaURL = "/plugins/Subsite/Subsites/" + subsite;
                }
                $.ajax({
                    url: contentAreaURL + "/textFiles/" + path + "_content" + (index + 1) + ".html?version=" + current.toUTCString(),
                    dataType: "text",
                    success: function (data) {
                        $("#content" + (index + 1)).html(data);
                    }
                });
            });
        }
    });
});

// Gets query string values //
function gup(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}