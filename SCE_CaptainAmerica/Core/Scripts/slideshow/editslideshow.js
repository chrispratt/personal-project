﻿/*
 ________        __  __    __             ______   __  __        __                      __                                                    
/        |      /  |/  |  /  |           /      \ /  |/  |      /  |                    /  |                                                   
$$$$$$$$/   ____$$ |$$/  _$$ |_         /$$$$$$  |$$ |$$/   ____$$ |  ______    _______ $$ |____    ______   __   __   __         __   _______ 
$$ |__     /    $$ |/  |/ $$   |        $$ \__$$/ $$ |/  | /    $$ | /      \  /       |$$      \  /      \ /  | /  | /  |       /  | /       |
$$    |   /$$$$$$$ |$$ |$$$$$$/         $$      \ $$ |$$ |/$$$$$$$ |/$$$$$$  |/$$$$$$$/ $$$$$$$  |/$$$$$$  |$$ | $$ | $$ |       $$/ /$$$$$$$/ 
$$$$$/    $$ |  $$ |$$ |  $$ | __        $$$$$$  |$$ |$$ |$$ |  $$ |$$    $$ |$$      \ $$ |  $$ |$$ |  $$ |$$ | $$ | $$ |       /  |$$      \ 
$$ |_____ $$ \__$$ |$$ |  $$ |/  |      /  \__$$ |$$ |$$ |$$ \__$$ |$$$$$$$$/  $$$$$$  |$$ |  $$ |$$ \__$$ |$$ \_$$ \_$$ |__     $$ | $$$$$$  |
$$       |$$    $$ |$$ |  $$  $$/       $$    $$/ $$ |$$ |$$    $$ |$$       |/     $$/ $$ |  $$ |$$    $$/ $$   $$   $$//  |    $$ |/     $$/ 
$$$$$$$$/  $$$$$$$/ $$/    $$$$/         $$$$$$/  $$/ $$/  $$$$$$$/  $$$$$$$/ $$$$$$$/  $$/   $$/  $$$$$$/   $$$$$/$$$$/ $$/__   $$ |$$$$$$$/  
                                                                                                                           /  \__$$ |          
                                                                                                                           $$    $$/           
                                                                                                                            $$$$$$/            
    Created By: Josh Sorensen
    Modified: April 2017 by Josh Sorensen

*/

$(document).ready(function () {
    // Loads form with dropdown of slideshows and buttons to save, cancel, or delete the slideshow //
    var beforeStr = ''
    + '<div class="alert alert-success" role="alert" style="display: none;"></div>'
    + '<div class="alert alert-danger" role="alert" style="display: none;"></div>'
    + '<form class="form-horizontal" method="post" id="editSlideshow" action="../../Core/Scripts/slideshow/EditSlideshow.aspx">'
    + '<div class="form-group"><label for="[name=\'navbars\']" class="col-sm-2 control-label">Select Slideshow</label><div class="col-sm-8"><select class="form-control" name="slideshows"></select></div></div>'
    + '<div class="form-group" id="newSlideInput"><label for="[name=\'slideshowNewName\']" class="col-sm-2 control-label">Slideshow Name</label><div class="col-sm-8"><input class="form-control" name="newSlide" type="text" /></div></div>'
    + '<div class="col-sm-offset-2 col-sm-8"><input type="button" id="delete" value="Delete Slideshow" class="btn btn-danger" onclick="deleteSlideshow()" /><br/><br/></div>'
    + '<div class="slideshowitems"></div>'
    + '<div class="form-group" id="slideshowSubmit">'
    + '<div class="col-sm-offset-2 col-sm-8">'
    + '<input type="submit" id="slideshowSave" value="Save Changes" class="btn btn-success" />'
    + '<input type="button" id="cancel" value="Cancel" class="btn btn-danger" />'
    + '</div></div></form>';
    $(".plugin-slideshow-edit").html(beforeStr);

    // Loads all slideshows lists //
    $.getJSON("/api/Slideshow/get/5", function (value) {
        $("select[name='slideshows']").append('<option value="new">Create New Slideshow</option>');
        // Checks if any slideshows exist //
        if (value.length > 0) {
            // Loads the slideshows into the dropdown list //
            for (var i = 0; i < value.length; i++) {
                if (value[i] != "subsiteDefault.json")
                    $("select[name='slideshows']").append('<option value="' + value[i].replace('.json', '') + '">' + value[i].replace('.json', '') + '</option>');
            }
            // Checks for a main.json slideshow file, if there is no main.json file, by default the next slideshow on the list appears //
            if (jQuery.inArray("main.json", value) !== -1) {
                $("select[name='slideshows']").val("main");
                $("#delete").prop("disabled", "");
                $("#newSlideInput").css('display', 'none');
                loadForm();
            }
            else {
                $("select[name='slideshows']").prop("selectedIndex", 1);
                $("#delete").prop("disabled", "");
                $("#newSlideInput").css('display', 'none');
                loadForm();
            }
        }
        else {
            // Option to create a new slideshow is selected by default with the textbox appearing as well //
            $("select[name='slideshows']").val("new");
            $("#delete").prop("disabled", "disabled");
            $("#newSlideInput").css('display', 'block');
            loadNewForm();
        }
    });

    // Checks for errors //
    if (gup("error") != "") {
        $(".alert-danger").css("display", "block");
        $(".alert-danger").html(gup("error"));
    }

    // Successful changes //
    if (gup("success") != "") {
        $(".alert-success").css("display", "block");
        $(".alert-success").html(gup("success"));
    }

    // When the slideshow dropdown is changed, this funtion runs //
    $("select[name='slideshows']").change(function () {
        // If the slideshow dropdown value is a new slideshow, the textbox with the name of the slideshow appears //
        if ($("select[name='slideshows']").val() == "new") {
            $("#newSlideInput").css('display', 'block');
            $("#delete").prop("disabled", "disabled");
            loadNewForm();
        }
        else {
            $("#newSlideInput").css('display', 'none');
            $("#delete").prop("disabled", "");
            loadForm();
        }
    });
});

function loadForm() {
    // Checks for authorized user //
    $.getJSON("/api/Auth/Get/5", function (value) {
        if (value == true) {
            var currentDate = new Date();

            // Loads slideshow items onto page with their associated values //
            $.getJSON("../../Core/Scripts/slideshow/slideshows/" + $("select[name='slideshows']").val() + ".json?version=" + currentDate.toUTCString(), function (data) {//?version=" + currentDate.toUTCString(), function (data) {
                var htmlStr = "";

                for (var i = 0; i < data.Items.length; i++) {

                    htmlStr += "<div id='tableRow" + i + "'><div class='form-group'><div class='col-sm-offset-2 col-sm-8'><h2>Slideshow item " + (i + 1) + "</h2></div></div>" +
                        "<div class='form-group'><label for='[name=\"header" + i + "\"]' class='col-sm-2 control-label'>Header</label><div class='col-sm-8'><input class='form-control' type='text' name='header" + i + "' value='" + data.Items[i].Heading + "' /></div></div>" +
                        "<div class='form-group'><label for='[name=\"description" + i + "\"]' class='col-sm-2 control-label'>Description</label><div class='col-sm-8'><textarea class='form-control' rows='6' cols='110' name='description" + i + "' maxlength='570'>" + data.Items[i].Description + "</textarea></div></div>" +
                        "<div class='form-group'><label for='[name=\"imagePath" + i + "\"]' class='col-sm-2 control-label'>Image Path</label><div class='col-sm-8'><input class='form-control' type='text' name='imagePath" + i + "' value='" + data.Items[i].ImagePath + "' /></div></div>" +
                        "<div class='form-group'><label for='[name=\"link" + i + "\"]' class='col-sm-2 control-label'>Link</label><div class='col-sm-8'><input class='form-control' type='text' name='link" + i + "' value='" + data.Items[i].Link + "' /></div></div>" +
                        "<div class='form-group'><div class='col-sm-offset-2 col-sm-8'><button type='button' id='delete" + i + "' class='btn btn-danger' onclick='deleteSlide(this.id)'>Remove Slide " + (i + 1) + "</button></div></div><hr/></div>";

                    if (i == data.Items.length - 1) {
                        htmlStr += "<div class='form-group remove'><div class='col-sm-offset-2 col-sm-8'><button type='button' id='add" + i + "' class='btn btn-default' onclick='addSlide(this.id)'><span class='glyphicon glyphicon-plus'></span>Add Slide</button></div><hr/></div>";
                    }
                }

                //$("#contentEdit").append(htmlStr);
                $(".slideshowitems").html(htmlStr);

            });
            
            // Sends admin to home page //
            $("#cancel").click(function () {
                window.location.href = "/";
            });
        }
        else {
            // Anonymous users are sent back to home page //
            alert("You are not authorized to access this page.");
            window.location.href = "/";
        }
    });
}

// The new slideshow form is loaded here //
function loadNewForm() {
    var htmlStr = "";
    var i = 0;

    htmlStr += "<div id='tableRow" + i + "'><div class='form-group'><div class='col-sm-offset-2 col-sm-8'><h2>Slideshow item " + (i + 1) + "</h2></div></div>" +
                        "<div class='form-group'><label for='[name=\"header" + i + "\"]' class='col-sm-2 control-label'>Header</label><div class='col-sm-8'><input class='form-control' type='text' name='header" + i + "' value='' /></div></div>" +
                        "<div class='form-group'><label for='[name=\"description" + i + "\"]' class='col-sm-2 control-label'>Description</label><div class='col-sm-8'><textarea class='form-control' rows='6' cols='110' name='description" + i + "' maxlength='570'></textarea></div></div>" +
                        "<div class='form-group'><label for='[name=\"imagePath" + i + "\"]' class='col-sm-2 control-label'>Image Path</label><div class='col-sm-8'><input class='form-control' type='text' name='imagePath" + i + "' value='' /></div></div>" +
                        "<div class='form-group'><label for='[name=\"link" + i + "\"]' class='col-sm-2 control-label'>Link</label><div class='col-sm-8'><input class='form-control' type='text' name='link" + i + "' value='' /></div></div>" +
                        "<div class='form-group'><div class='col-sm-offset-2 col-sm-8'><button type='button' id='delete" + i + "' class='btn btn-danger' onclick='deleteSlide(this.id)'>Remove Slide " + (i + 1) + "</button></div></div><hr/></div>"
                        "<div class='form-group remove'><div class='col-sm-offset-2 col-sm-8'><button type='button' id='add" + i + "' class='btn btn-default' onclick='addSlide(this.id)'><span class='glyphicon glyphicon-plus'></span>Add Slide</button></div><hr/></div>";

    $(".slideshowitems").html(htmlStr);
}

// Adds new slide to slideshow //
function addSlide(id) {
    // Removes add slide button on last id //
    $("#" + id + ", .remove").remove();
    id = id.substr(3, id.length);
    // Adds to last id //
    newid = Number(id) + 1;

    var htmlStr = "<div id='tableRow" + newid + "'><div class='form-group'><div class='col-sm-offset-2 col-sm-8'><h2>Slideshow item " + (newid + 1) + "</h2></div></div>" +
                        "<div class='form-group'><label for='[name=\"header" + newid + "\"]' class='col-sm-2 control-label'>Header</label><div class='col-sm-8'><input class='form-control' type='text' name='header" + newid + "' /></div></div>" +
                        "<div class='form-group'><label for='[name=\"description" + newid + "\"]' class='col-sm-2 control-label'>Description</label><div class='col-sm-8'><textarea class='form-control' rows='6' cols='110' name='description" + newid + "' maxlength='570'></textarea></div></div>" +
                        "<div class='form-group'><label for='[name=\"imagePath" + newid + "\"]' class='col-sm-2 control-label'>Image Path</label><div class='col-sm-8'><input class='form-control' type='text' name='imagePath" + newid + "' /></div></div>" +
                        "<div class='form-group'><label for='[name=\"link" + newid + "\"]' class='col-sm-2 control-label'>Link</label><div class='col-sm-8'><input class='form-control' type='text' name='link" + newid + "' /></div></div>" +
                        "<div class='form-group'><div class='col-sm-offset-2 col-sm-8'><button type='button' id='delete" + newid + "' class='btn btn-danger' onclick='deleteSlide(this.id)'>Remove Slide " + (newid + 1) + "</button></div></div><hr/></div>";

    htmlStr += "<div class='form-group remove'><div class='col-sm-offset-2 col-sm-8'><button type='button' id='add" + newid + "' class='btn btn-default' onclick='addSlide(this.id)'><span class='glyphicon glyphicon-plus'></span>Add Slide</button></div><hr/></div>";

    $(".slideshowitems").append(htmlStr);
}

function deleteSlide(id) {

    // Deletes selected slide //
    $("#" + id + ", .remove").remove();
    id = id.substr(6, id.length);

    $("#editSlideshow #tableRow" + id).remove();
}

// This function deletes the whole slideshow //
function deleteSlideshow() {
    $.ajax({
        method: "DELETE",
        url: "/api/Slideshow/Delete/" + $("select[name='slideshows']").val(),
        success: function (data) {
            if (!data) {
                // If the delete was not successful, an error is returned //
                window.location.href = "/Main/EditSlideshow?error=An error occurred while deleting the slideshow " + $("select[name='slideshows']").val() + ".";
            }
            else {
                window.location.reload();
            }
        }
    });
}