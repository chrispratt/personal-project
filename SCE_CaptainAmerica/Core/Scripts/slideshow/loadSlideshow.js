﻿var url = window.location.href; // Gets the location url of current page
var path = window.location.pathname; // Gets the location path of current page
var subsite = "Main";

var realPath = path.split("/");
path = realPath[realPath.length - 1];
if (realPath.length > 2) {
    subsite = realPath[realPath.length - 2];
}

$(document).ready(function () {
    var currDate = new Date();
    if (subsite == "Main") {
        // Loads the slideshow in json format from the "slideshow.json" file //
        $.getJSON("/Core/Scripts/slideshow/slideshows/" + $("#slideshowWrapper").attr("data-slideshow") + ".json?version=" + currDate.toUTCString(), function (data) {
            var carouselStr = "";
            var newsItems = '';
            var indicators = "";
            for (var i = 0; i < data.Items.length; i++) {
                // Each object is loaded into the bootstrap carousel along with the caption. //
                if (i == 0) {
                    carouselStr += '<div class="item active">' +
                                        '<div class="col-md-9 slideshow-image-container" style="padding-right: 0px; padding-left: 0px;">' +
                                            '<img class="slideshow-image" src="' + data.Items[i].ImagePath + '" alt="' + data.Items[i].Heading + '">' +
                                        '</div>' +
                                        '<div class="news-item col-md-3 slideshow-label-container" id="newsItem' + i + '">' +
                                            '<h2 style="text-align: center;"><a href="' + data.Items[i].Link + '">' + data.Items[i].Heading + '</a></h2><br/>' +
                                            '<p style="font-family: Georgia; font-size: 14px;">' + data.Items[i].Description + '</p>' +
                                            '<!-- Indicators -->' +
                                            '<ol class="carousel-indicators" id="slideshow-buttons' + i + '"></ol>' +
                                        '</div>' +
                                   '</div>';
                    indicators += '<li data-target="#slideshow" data-slide-to="' + i + '" class="active"></li>';
                }
                else {
                    carouselStr += '<div class="item">' +
                                        '<div class="col-md-9 slideshow-image-container" style="padding-right: 0px; padding-left: 0px;">' +
                                            '<img class="slideshow-image" src="' + data.Items[i].ImagePath + '" alt="' + data.Items[i].Heading + '">' +
                                        '</div>' +
                                        '<div class="news-item col-md-3 slideshow-label-container" id="newsItem' + i + '">' +
                                            '<h2 style="text-align: center;"><a href="' + data.Items[i].Link + '">' + data.Items[i].Heading + '</a></h2><br/>' +
                                            '<p style="font-family: Georgia; font-size: 14px;">' + data.Items[i].Description + '</p>' +
                                            '<!-- Indicators -->' +
                                            '<ol class="carousel-indicators" id="slideshow-buttons' + i + '"></ol>' +
                                        '</div>' +
                                    '</div>';
                    indicators += '<li data-target="#slideshow" data-slide-to="' + i + '"></li>';
                }
            }
            newsItems += '</div>';
            $(".carousel-inner").html(carouselStr);
            $("ol[id^='slideshow-buttons']").html(indicators);
            $("#content1").css('padding-right', '0px');

            // Starts the bootstrap carousel.  Also changes the caption whenever the picture changes. //

            $("#slideshow").carousel();
            $("#slideshow").bind('slid.bs.carousel', function (e) {
                var currentIndex = $('div.active').index();
                $("div[id^='newsItem']").each(function (index) {
                    if (index == currentIndex) {
                        $("[data-slide-to='" + index + "']").attr('class', 'active');
                    }
                    else {
                        $("[data-slide-to='" + index + "']").attr('class', 'none');
                    }
                });
            });
        });
    }
    else {
        // Loads the slideshow in json format from the "slideshow.json" file //
        $.getJSON("/Core/Scripts/slideshow/slideshows/" + subsite + "_" + $("#slideshowWrapper").attr("data-slideshow") + ".json?version=" + currDate.toUTCString(), function (data) {
            var carouselStr = "";
            var newsItems = '';
            var indicators = "";
            for (var i = 0; i < data.Items.length; i++) {
                // Each object is loaded into the bootstrap carousel along with the caption. //
                if (i == 0) {
                    carouselStr += '<div class="item active">' +
                                        '<div class="col-md-9 slideshow-image-container" style="padding-right: 0px; padding-left: 0px;">' +
                                            '<img class="slideshow-image" src="' + data.Items[i].ImagePath + '" alt="' + data.Items[i].Heading + '">' +
                                        '</div>' +
                                        '<div class="news-item col-md-3 slideshow-label-container" id="newsItem' + i + '">' +
                                            '<h2 style="text-align: center;"><a href="' + data.Items[i].Link + '">' + data.Items[i].Heading + '</a></h2><br/>' +
                                            '<p style="font-family: Georgia; font-size: 14px;">' + data.Items[i].Description + '</p>' +
                                            '<!-- Indicators -->' +
                                            '<ol class="carousel-indicators" id="slideshow-buttons' + i + '"></ol>' +
                                        '</div>' +
                                   '</div>';
                    indicators += '<li data-target="#slideshow" data-slide-to="' + i + '" class="active"></li>';
                }
                else {
                    carouselStr += '<div class="item">' +
                                        '<div class="col-md-9 slideshow-image-container" style="padding-right: 0px; padding-left: 0px;">' +
                                            '<img class="slideshow-image" src="' + data.Items[i].ImagePath + '" alt="' + data.Items[i].Heading + '">' +
                                        '</div>' +
                                        '<div class="news-item col-md-3 slideshow-label-container" id="newsItem' + i + '">' +
                                            '<h2 style="text-align: center;"><a href="' + data.Items[i].Link + '">' + data.Items[i].Heading + '</a></h2><br/>' +
                                            '<p style="font-family: Georgia; font-size: 14px;">' + data.Items[i].Description + '</p>' +
                                            '<!-- Indicators -->' +
                                            '<ol class="carousel-indicators" id="slideshow-buttons' + i + '"></ol>' +
                                        '</div>' +
                                    '</div>';
                    indicators += '<li data-target="#slideshow" data-slide-to="' + i + '"></li>';
                }
            }
            newsItems += '</div>';
            $(".carousel-inner").html(carouselStr);
            $("ol[id^='slideshow-buttons']").html(indicators);
            $("#content1").css('padding-right', '0px');

            // Starts the bootstrap carousel.  Also changes the caption whenever the picture changes. //

            $("#slideshow").carousel();
            $("#slideshow").bind('slid.bs.carousel', function (e) {
                var currentIndex = $('div.active').index();
                $("div[id^='newsItem']").each(function (index) {
                    if (index == currentIndex) {
                        $("[data-slide-to='" + index + "']").attr('class', 'active');
                    }
                    else {
                        $("[data-slide-to='" + index + "']").attr('class', 'none');
                    }
                });
            });
        });
    }
});