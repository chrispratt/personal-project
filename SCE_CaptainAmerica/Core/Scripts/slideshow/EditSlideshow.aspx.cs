﻿/*
 ________        __  __    __       ______   __  __        __                      __                               
/        |      /  |/  |  /  |     /      \ /  |/  |      /  |                    /  |                              
$$$$$$$$/   ____$$ |$$/  _$$ |_   /$$$$$$  |$$ |$$/   ____$$ |  ______    _______ $$ |____    ______   __   __   __ 
$$ |__     /    $$ |/  |/ $$   |  $$ \__$$/ $$ |/  | /    $$ | /      \  /       |$$      \  /      \ /  | /  | /  |
$$    |   /$$$$$$$ |$$ |$$$$$$/   $$      \ $$ |$$ |/$$$$$$$ |/$$$$$$  |/$$$$$$$/ $$$$$$$  |/$$$$$$  |$$ | $$ | $$ |
$$$$$/    $$ |  $$ |$$ |  $$ | __  $$$$$$  |$$ |$$ |$$ |  $$ |$$    $$ |$$      \ $$ |  $$ |$$ |  $$ |$$ | $$ | $$ |
$$ |_____ $$ \__$$ |$$ |  $$ |/  |/  \__$$ |$$ |$$ |$$ \__$$ |$$$$$$$$/  $$$$$$  |$$ |  $$ |$$ \__$$ |$$ \_$$ \_$$ |
$$       |$$    $$ |$$ |  $$  $$/ $$    $$/ $$ |$$ |$$    $$ |$$       |/     $$/ $$ |  $$ |$$    $$/ $$   $$   $$/ 
$$$$$$$$/  $$$$$$$/ $$/    $$$$/   $$$$$$/  $$/ $$/  $$$$$$$/  $$$$$$$/ $$$$$$$/  $$/   $$/  $$$$$$/   $$$$$/$$$$/  
                                                                                                                    
                                                                                                                    
    Created By: Josh Sorensen
    Modified: September 2016                                                                                                                    
 */

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SCE_batman
{
    public partial class EditSlideshow : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            // Checks if user is logged in //
            if (HttpContext.Current.User.Identity.IsAuthenticated)
            {
                // Gets all the values from the Request Form and puts them in lists //
                List<string> headers = new List<string>();
                List<string> description = new List<string>();
                List<string> link = new List<string>();
                List<string> imagePath = new List<string>();
                string slideshow = "";
                string newSlideshowName = "";
                foreach (string header in Request.Form.AllKeys)
                {
                    // Regex here to prevent cross-site scripting //
                    Regex rRemScript = new Regex(@"<script[^>]*>[\s\S]*?</script>");
                    var output = rRemScript.Replace(Request.Form[header], "");
                    output = output.Replace("\\", "\\\\");
                    if (header.StartsWith("header"))
                    {
                        headers.Add(output);
                    }
                    else if (header.StartsWith("description"))
                    {
                        description.Add(output.Replace("\"", "\\\"").Replace("\r\n", "<br/>").Replace("<script>*</script>", ""));
                    }
                    else if (header.StartsWith("link"))
                    {
                        link.Add(output);
                    }
                    else if (header.StartsWith("imagePath"))
                    {
                        imagePath.Add(output);
                    }
                    else if (header.StartsWith("slideshows"))
                    {
                        slideshow = output;
                    }
                    else if (header.StartsWith("newNav"))
                    {
                        newSlideshowName = output;
                    }
                }

                // Changes the slideshow file name to the slideshow file being modified //
                string fileName = slideshow + ".json";

                // If a new slideshow is being created, it checks to see if the slideshow already exists //
                if (slideshow == "new")
                {
                    fileName = newSlideshowName + ".json";
                    if (File.Exists(System.Web.HttpContext.Current.Server.MapPath("~/Core/Scripts/slideshow/slideshows/" + fileName)))
                    {
                        // If the slideshow exists, an error is returned //
                        Response.Redirect("~/Main/EditSlideshow?error=A navigation with this name already exists!");
                    }
                }

                string json = "{ \"Items\": [";

                // Iterates through the lists and adds them to the json string //
                for (int i = 0; i < headers.Count; i++)
                {
                    if (i == headers.Count - 1)
                    {
                        json += "{ \"Heading\": \"" + headers[i] + "\", \"Description\": \"" + description[i] + "\", \"Link\": \"" + link[i] + "\", \"ImagePath\": \"" + imagePath[i] + "\" }\n";

                    }
                    else
                    {
                        json += "{ \"Heading\": \"" + headers[i] + "\", \"Description\": \"" + description[i] + "\", \"Link\": \"" + link[i] + "\", \"ImagePath\": \"" + imagePath[i] + "\" },\n";
                    }
                }
                json += "] }";

                // Saves slideshow to file //
                var path = Server.MapPath("~/Core/Scripts/slideshow/slideshows/" + fileName);
                System.IO.File.WriteAllText(path, json);

                Response.Redirect("~/Main/EditSlideshow?success=The slideshow was saved successfully!");
            }
            else
            {
                Response.Redirect("~/Main/EditSlideshow?error=There was an error while saving the slideshow!");
            }
        }
    }
}