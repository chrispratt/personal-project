﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace SCE_CaptainAmerica.plugins.slideshow.Controllers
{
    public class SlideshowController : ApiController
    {
        // GET: api/Slideshow
        // Don't worry about this function //
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/Slideshow/5
        public string[] Get(string id)
        {
            // Sets the id to be the directory where the slideshows are listed //
            id = "~/Core/Scripts/slideshow/slideshows";
            if (HttpContext.Current.User.Identity.IsAuthenticated)
            {
                // Creates of list of slideshows and returns them //
                List<string> files = new List<string>();
                DirectoryInfo dirInfo = new DirectoryInfo(System.Web.HttpContext.Current.Server.MapPath(id));
                if (dirInfo.Exists)
                {
                    foreach (FileInfo fInfo in dirInfo.GetFiles())
                    {
                        files.Add(fInfo.Name);
                    }
                    return files.ToArray();
                }
                else
                {
                    return null;
                }
            }
            else
            {
                return null;
            }
        }

        // POST: api/Slideshow
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/Slideshow/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/Slideshow/5
        public bool Delete(string id)
        {
            // Checks if the slideshow exists //
            if (File.Exists(System.Web.HttpContext.Current.Server.MapPath("~/Core/Scripts/slideshow/slideshows/" + id + ".json")))
            {
                // If the slideshow exists, deletes the slideshow //
                File.Delete(System.Web.HttpContext.Current.Server.MapPath("~/Core/Scripts/slideshow/slideshows/" + id + ".json"));
                return true;
            }
            return false;
        }
    }
}
