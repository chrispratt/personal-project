﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;

namespace SCE_CaptainAmerica.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Pages(string id, string subsite)
        {
            string subsiteStr = id;
            if (id == "" || id == null)
            {
                if (subsite == null || subsite == "")
                {
                    subsiteStr = "Index";
                }
                else
                {
                    subsiteStr = subsite;
                }
            }
            else
            {
                if (subsite != "Main")
                {
                    if (System.IO.File.Exists(HttpContext.Server.MapPath("~/plugins/Subsite/Subsites/" + subsite + "/Views/Home/" + id + ".cshtml")))
                    {
                        subsiteStr = "~/plugins/Subsite/Subsites/" + subsite + "/Views/Home/" + id + ".cshtml";
                    }
                }
            }

            // Try to find the requested view and return a 404 if it's not found.
            ViewEngineResult RequestedView = ViewEngines.Engines.FindView(ControllerContext, subsiteStr, null);
            if (RequestedView.View == null)
            {
                Response.TrySkipIisCustomErrors = true;
                Response.StatusCode = 404;
                return View("PageNotFound");
            }

            return View(subsiteStr);
        }
    }
}