﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace SCE_CaptainAmerica.Controllers
{
    public class AuthController : ApiController
    {
        // GET: api/Auth
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/Auth/5
        public bool Get(string id)
        {
            if (HttpContext.Current.User.Identity.IsAuthenticated)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        [AcceptVerbs("GET")]
        // GET: api/Auth/5
        public string userName(string id)
        {
            if (HttpContext.Current.User.Identity.IsAuthenticated)
            {
                return HttpContext.Current.User.Identity.Name;
            }
            else
            {
                return null;
            }
        }

        [AcceptVerbs("GET")]
        // GET: api/Auth/5
        // Will be used in the future, will be used to load all site pages into a list //
        public string[] filesLink(string id)
        {
            id = "~/Views/Home";
            // Checks for authorized user //
            if (HttpContext.Current.User.Identity.IsAuthenticated)
            {
                // Loads pages and returns them in array to user //
                List<string> files = new List<string>();
                DirectoryInfo dirInfo = new DirectoryInfo(System.Web.HttpContext.Current.Server.MapPath(id));
                if (dirInfo.Exists)
                {
                    foreach (FileInfo fInfo in dirInfo.GetFiles())
                    {
                        files.Add(fInfo.Name);
                    }
                    return files.ToArray();
                }
                else
                {
                    return null;
                }
            }
            else
            {
                return null;
            }
        }

        // POST: api/Auth
        // Used to add new content areas //
        public void Post(string id)
        {
            // Checks to see if page is a subsite //
            if (id.Split('_').Count() <= 2)
            {
                // If the page is not a subsite, checks if content exists //
                string file = System.Web.HttpContext.Current.Server.MapPath("~/textfiles/" + id + ".html");
                if (!File.Exists(file))
                {
                    // If content does not exist, create a new content area html file //
                    var path = System.Web.HttpContext.Current.Server.MapPath("~/textfiles/" + id + ".html");
                    System.IO.File.WriteAllText(file, "");
                }
            }
            else
            {
                // Checks if content exists //
                string file = System.Web.HttpContext.Current.Server.MapPath("~/plugins/Subsite/Subsites/" + id.Split('_')[0] + "/textfiles/" + id.Split('_')[1] + '_' + id.Split('_')[2] + ".html");
                if (!File.Exists(file))
                {
                    // If content does not exist, create a new content area html file //
                    var path = System.Web.HttpContext.Current.Server.MapPath("~/plugins/Subsite/Subsites/" + id.Split('_')[0] + "/textfiles/" + id.Split('_')[1] + '_' + id.Split('_')[2] + ".html");
                    System.IO.File.WriteAllText(file, "");
                }
            }
        }

        // PUT: api/Auth/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/Auth/5
        public void Delete(int id)
        {
        }
    }
}
