﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.DirectoryServices.AccountManagement;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SCE_batman.Login
{
    public partial class Login : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            // Gets the BYU domain //
            PrincipalContext ctx = new PrincipalContext(ContextType.Domain, "BYU");

            // Finds netId in BYU domain //
            UserPrincipal user = UserPrincipal.FindByIdentity(ctx, HttpContext.Current.User.Identity.Name);

            // Gets groups in BYU domain //
            GroupPrincipal group = GroupPrincipal.FindByIdentity(ctx, "fhss-sharepointstudentadmins");
            GroupPrincipal group2 = GroupPrincipal.FindByIdentity(ctx, "fhss-csr");

            // Gets list of admins from admins.txt file //
            string ol = "";
            // Check to see if list of admins exists //
            if (File.Exists(System.Web.HttpContext.Current.Server.MapPath("~/Login/admins.txt")))
            {
                ol = File.ReadAllText(System.Web.HttpContext.Current.Server.MapPath("~/Login/admins.txt"));
            }
            else
            {
                // Creates the list of admins, but no names are in it //
                System.IO.File.WriteAllText(System.Web.HttpContext.Current.Server.MapPath("~/Login/admins.txt"), "");
                ol = File.ReadAllText(System.Web.HttpContext.Current.Server.MapPath("~/Login/admins.txt"));
            }
            string[] admins = ol.Split(',');

            // Checks if the netId is part of the BYU domain //
            if (user != null)
            {
                // Checks if user is in the BYU domain groups or in the admins.txt file //
                if (user.IsMemberOf(group) || user.IsMemberOf(group2) || Array.IndexOf(admins, HttpContext.Current.User.Identity.Name) >= 0)
                {
                    // They are authenticated and redirected to their page //
                    Response.Redirect(Request["ReturnURL"]);
                }
                else
                {
                    // The user is signed out and redirected to their page //
                    FormsAuthentication.SignOut();
                    Response.Redirect(Request["ReturnURL"]);
                }
            }
            else
            {
                // The user is signed out and redirected to their page //
                FormsAuthentication.SignOut();
                Response.Redirect(Request["ReturnURL"]);
            }
        }
    }
}